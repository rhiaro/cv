# Preamble

I enjoy defining my own research in the area of web science. A hacker at heart, I like to start prototyping technical solutions so that I can begin applying them to real world problems as quickly as possible. I love to work with web technologies, both front-end and server-side, and Python is my weapon of choice. I'm excited about using (linked) data in a smart, connected world to empower individuals, and finding ways to make societies (online and offline) more sustainable.

**Keywords:** Communities, creative collaboration, decentralised systems, empowerment, linked data, open data, social machines, social network analysis, semantic web, user-generated content, web science.

# Education

## PhD in Informatics (October 2012 - present)

### University of Edinburgh, Centre for Intelligent Systems and their Applications. 

Examining the utility of semantic web technologies for describing content creators, facilitating collaborative creative tasks, and distribution of the resulting multimedia outputs. 

## MSc by Research Interdisciplinary Creative Practices (2011 - 2012)

### University of Edinburgh

A web-based authoring system for location-sensitive text landscapes and Interactive Fiction.

## BSC (Hons) Web Technology (2008 - 2011)

### University of Lincoln

Graduated with a First Class degree and Award for Outstanding Contribution from the School of Computer Science.

## Open University

### Short course Linux: An Introduction (2010)

### Short course Digital Photography (2007)

# Publications and prizes

## Constructed Identity and Social Machines: A Case Study in Creative Media Production. 

### In _WWW 2014 Companion_, Seoul (forthcoming). ACM.

## Amateur Creative Digital Content on the Semantic Web. 

### Poster at _10th Summer School for Ontology Engineering and the Semantic Web_ (2013).

## CakeBomb: an online collaborative community. 

### Poster at _BCS Lovelace Colloquium_ (2011). Winner of runner up prize for Original Projects

## Google Writer: a mash-up. 

### Poster at _BCS Lovelace Colloquium_ (2010). Winner of runner up prize for People's Choice

# Presentations

## Constructed Identity and Social Machines: A Case Study in Creative Media Production

### Presentation at University of Edinburgh Social Informatics Cluster (2014)

## Digital media on the Semantic Web

### Pecha Kucha at Digital Methods as a Mainstream Methodology (2012)

## Palimpsest: Location-aware literature

### Presentation and panellist on 'Writing Different Together' at Remediating the Social (2012)


# Employment and Experience

## TA and Marker (2014 - present)

### University of Edinburgh

For undergrad/masters course Multi-Agent Semantic Web Systems.

## Project supervisor (2013)

### University of Edinburgh

'Context-aware Web Design' for Digital Media Studio Project (part of taught MSc Design & Digital Media).

## Tutor and demonstrator (2012 & 2013)

### University of Edinburgh

For Dynamic Web Design (part of taught MSc Design & Digital Media).

## Software Developer for Community Hacking (2012 - 2013)

### University of Edinburgh

Mostly PHP and front-end development for a research project focusing on community engagement through hyperlocal digital news and physical art installations.

## Google University Programmes Intern (2010)
### Google, London

Working with a team to organise university and diversity events. Developing internal administration systems and websites and providing technical training for staff.

## Casual Staff in Marketing and Communications (2009 - 2012)

### University of Lincoln Students' Union

Developing administration systems and websites for internal and external use, and training staff to use them.


# Voluntary positions

## Co-founder and Coordinator (2013 - present)

### Prewired: under 19s programming club (prewired.org)

## Community Coordinator Edinburgh (2013 - present)

### Open Knowledge Foundation Scotland (scot.okfn.org)

## Assistant Centre Lead (2013) and Mentor (2012 and 2013)

### Young Rewired State, Edinburgh Centre

A UK-wide hack week for under 19s (youngrewiredstate.org).

## International Open Data Day Edinburgh Co-organiser (2013)

## Smart Data Hack main organiser (2013)

A widely acclaimed week-long hack for undergraduates at the University of Edinburgh.

## Poster competition judge (2012 and 2013)

### BCS Lovelace Colloquium

## University Societies

### President, University of Edinburgh SocieTea (2012 - present)

### Founding President, University of Lincoln Computing Society (2010 - 2011)

### Communications Officer, University of Lincoln Sci-Fi, Fantasy & Horror Society (2009 - 2011)

## Student Ambassador, Student as Producer (2010 - 2011)

## Tutor, fund-raiser and general volunteer (2009)

### People First India

I taught computer skills to young adults in Bihar, India, working around the language barrier and frequent power cuts.

# Other interests

## The Web as a Force For Good

I'm keen to learn anything about web technologies being used to improve peoples' every day experiences, and empowerment as society as a whole. I'm involved with open data initiatives and experimenting with personal data stores, 'unhosted' web applications and other decentralised systems with this in mind.

## Sustainability

I volunteer for Edinburgh's Swap and Reuse Hub (SHRUB - shrubcoop.org) and Edinburgh Freegle, as I like to get involved with initiatives aimed and reducing waste and production of physical things through upcycling and repurposing.

## Making something from nothing

Including science fiction and fantasy stories (I've taken part in National Novel Writing Month every year since 2008 with varying degrees of success).

I also enjoy knitting, crochet and baking, and am gradually learning to make clothes from scratch.

## Parrot behaviour and animal-computer interaction

An ongoing (but slow) project to develop a parrot-friendly interface for controlling music, using RFID tags and plastic toys. And training the parrot to use it of course.